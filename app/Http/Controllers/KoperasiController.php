<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KoperasiModel;
use App\JenisKoperasiModel;
use App\RekapFileModel;
use Auth;
use File;
use App\Http\Requests\KoperasiRequestStore;

class KoperasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uniqueFilename($path, $name, $ext) {
	
        $output = $name;
        $basename = basename($name, '.' . $ext);
        $i = 2;
        
        while(File::exists($path . '/' . $output)) {
            $output = $basename . $i . '.' . $ext;
            $i ++;
        }
        
        return $output;
        
    }    
    public function index()
    {
        $jenisKoperasi = JenisKoperasiModel::all();
        return view('admin.koperasi.index',['jenisKoperasi' => $jenisKoperasi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenisKoperasi = JenisKoperasiModel::all();
        return view('admin.koperasi.create',['jenisKoperasi' => $jenisKoperasi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KoperasiRequestStore $request)
    {
        try {            
            $koperasi = new KoperasiModel;
            $koperasi->jenis_id = $request->input('jenis_id');
            $koperasi->users_id = Auth::user()->id;
            $koperasi->nomor_spk = $request->input('nomor_spk');
            $koperasi->tgl_spk = $request->input('tgl_spk');
            $koperasi->nama_institusi = $request->input('nama_institusi');
            $koperasi->nama_ketua = $request->input('nama_ketua');
            $koperasi->alamat = $request->input('alamat');
            $koperasi->jml_anggota = $request->input('jml_anggota');
            $koperasi->biaya_hosting = $request->input('biaya_hosting');
            $koperasi->pic_teknik = $request->input('pic_teknik');
            $koperasi->no_telp = $request->input('no_telp');
            $koperasi->email = $request->input('email');
            $koperasi->url = $request->input('url');
            $koperasi->tgl_setup = $request->input('tgl_setup');
            $koperasi->harga_aplikasi = $request->input('harga_aplikasi');
            $koperasi->history = $request->input('history');
            $koperasi->status_pembayaran = $request->input('status_pembayaran');
            $koperasi->save();
            
            return redirect('koperasi')->with('success','Data Berhasil di Masukan');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jenisKoperasi = JenisKoperasiModel::with(['koperasi','users'])->findOrFail($id);        
        return view('admin.koperasi.show',['jenisKoperasi' => $jenisKoperasi]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {        
        $koperasi = KoperasiModel::with(['jenis','rekap_file'])->findOrFail($id);
        $jenis = JenisKoperasiModel::all();        
        return view('admin.koperasi.edit',[
            'koperasi' => $koperasi,
            'jenis' => $jenis
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KoperasiRequestStore $request, $id)
    {
        try {
            $koperasi = KoperasiModel::with('jenis')->findOrFail($id);
            $koperasi->jenis_id = $request->input('jenis_id');
            $koperasi->users_id = Auth::user()->id;
            $koperasi->nomor_spk = $request->input('nomor_spk');
            $koperasi->tgl_spk = $request->input('tgl_spk');
            $koperasi->nama_institusi = $request->input('nama_institusi');
            $koperasi->nama_ketua = $request->input('nama_ketua');
            $koperasi->alamat = $request->input('alamat');
            $koperasi->jml_anggota = $request->input('jml_anggota');
            $koperasi->biaya_hosting = $request->input('biaya_hosting');
            $koperasi->pic_teknik = $request->input('pic_teknik');    
            $koperasi->no_telp = $request->input('no_telp');
            $koperasi->email = $request->input('email');
            $koperasi->url = $request->input('url');
            $koperasi->tgl_setup = $request->input('tgl_setup');
            $koperasi->harga_aplikasi = $request->input('harga_aplikasi');
            $koperasi->history = $request->input('history');
            $koperasi->status_pembayaran = $request->input('status_pembayaran');

            $koperasi->save();

            return redirect('koperasi/'.$koperasi->jenis->id)->with('success', 'Data Berhasil di Perbaharui');         
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $koperasi = KoperasiModel::with('rekap_file')->findOrFail($id);
            foreach($koperasi->rekap_file as $value)
            {
                $path = public_path()."/files/".$value->nama;
                File::delete($path);
            }            
            $koperasi->delete();

            return back()->with('success','Data Berhasil di Hapus');
        } catch (Exception $e) {
            return back()->with('danger', $e->getMessage());
        }
    }
}

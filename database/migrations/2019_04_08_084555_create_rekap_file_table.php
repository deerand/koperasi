<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekapFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekap_file', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('koperasi_id')->unsigned();
            $table->foreign('koperasi_id')
            ->references('id')->on('koperasi')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->tinyInteger('users_id')->unsigned();
            $table->foreign('users_id')
            ->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('nama');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekap_file');
    }
}

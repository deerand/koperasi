@extends('layouts.app')
@section('title')
    Tambah Role
@endsection
@section('headerPage')
    Tambah Role
@endsection
@section('isi')
@if ($errors->any())    
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('success') }}
</div>
@endif
@if(session()->has('danger'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
        {{ session()->get('danger') }}
</div>
@endif
<form method="POST" action="{{ url('role')}}" enctype="multipart/form-data">
    @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="name" class="form-control" name="name" placeholder="Masukan Nama Role">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
</form>        

@endsection
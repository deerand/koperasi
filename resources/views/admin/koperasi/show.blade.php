@extends('layouts.app')
@section('title')
  Daftar Semua Koperasi {{$jenisKoperasi->nama}}
@endsection
@section('isi')
<div class="m-portlet m-portlet--head-lg">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon">
          <i class="la la-list"></i>
        </span>
        <h3 class="m-portlet__head-text">
          Daftar Semua Koperasi {{$jenisKoperasi->nama}}
        </h3>
      </div>
    </div>
  </div>
  <div class="m-portlet__body no-pedding">
    @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {{ session()->get('success') }}
    </div>
    @endif
    @if(session()->has('danger'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            {{ session()->get('danger') }}
    </div>
    @endif
    <div class="list-section">
      @forelse ($jenisKoperasi->koperasi as $item) 
      <div class="list-section__item">
        <div class="section__content image-box">
          <div class="section__desc">
            <h5 class="section__title">{{ $item->nama_institusi }}</h5>
            <div class="section__info">
              <div class="section__info__item sm-text">
                <span class="info__label">Posted By :</span>
                <a href="" class="info__detail m-link">{{ $item->users->name }}</a>
              </div>
            </div>
          </div>
        </div>
        <div class="section__action">
          <div class="list__section__action">
            <a href="{{ url('koperasi/'.$item->id.'/edit')}}" class="btn m-btn btn-success btn-sm m-btn--icon m-btn--air icon-only">
              <span>
                <i class="la la-pencil"></i>
                <span>Edit Koperasi</span>
              </span>
            </a>
            <a href="{{ url('file/'.$item->id) }}" class="btn m-btn btn-info btn-sm m-btn--icon m-btn--pill m-btn--air icon-only">
              <span>
                <i class="la la-file"></i>
                <span>Management File</span>
              </span>
            </a>            
            <a href="#" class="btn m-btn btn-primary btn-sm m-btn--icon m-btn--pill m-btn--air icon-only">
              <span>
                <i class="la la-eye"></i>
                <span>Profil Koperasi</span>
              </span>
            </a>
            <a href="javascript:void(0);" onclick="$(this).find('form').submit();" class="btn m-btn btn-outline-danger btn-sm  m-btn--icon m-btn--pill icon-only m_sweetalert_5">
              <span>
                <i class="la la-trash"></i>
                <span>Delete Koperasi</span>                
              </span>
              <form action="{{ route('koperasi.destroy', $item->id) }}" method="post">
                @csrf
                @method('DELETE')
              </form>
            </a>
          </div>
        </div>
      </div>
      @empty
      <div class="m-portlet__body">
        <p>Kosong</p>
      </div>        
      @endforelse            
    </div>
  </div>
</div>
@endsection
/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.30-MariaDB : Database - project_koperasi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
-- CREATE DATABASE /*!32312 IF NOT EXISTS*/`project_koperasi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `datasmar_project_koperasi`;

/*Table structure for table `jenis_koperasi` */

DROP TABLE IF EXISTS `jenis_koperasi`;

CREATE TABLE `jenis_koperasi` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` tinyint(3) unsigned NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jenis_koperasi_users_id_foreign` (`users_id`),
  CONSTRAINT `jenis_koperasi_users_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `jenis_koperasi` */

insert  into `jenis_koperasi`(`id`,`users_id`,`nama`,`image`,`created_at`,`updated_at`) values 
(1,1,'KSP','default_image.jpg','2019-04-10 07:52:40','2019-04-10 10:11:42'),
(2,1,'KSU','default_image.jpg','2019-04-10 07:52:47','2019-04-10 07:52:47');

/*Table structure for table `koperasi` */

DROP TABLE IF EXISTS `koperasi`;

CREATE TABLE `koperasi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `jenis_id` tinyint(3) unsigned NOT NULL,
  `users_id` tinyint(3) unsigned NOT NULL,
  `nomor_spk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_spk` date NOT NULL,
  `nama_institusi` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_ketua` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jml_anggota` mediumint(9) DEFAULT NULL,
  `biaya_hosting` int(11) NOT NULL,
  `pic_teknik` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_setup` date NOT NULL,
  `harga_aplikasi` int(11) NOT NULL,
  `history` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_pembayaran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `koperasi_jenis_id_foreign` (`jenis_id`),
  KEY `koperasi_users_id_foreign` (`users_id`),
  CONSTRAINT `koperasi_jenis_id_foreign` FOREIGN KEY (`jenis_id`) REFERENCES `jenis_koperasi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `koperasi_users_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `koperasi` */

insert  into `koperasi`(`id`,`jenis_id`,`users_id`,`nomor_spk`,`tgl_spk`,`nama_institusi`,`nama_ketua`,`alamat`,`jml_anggota`,`biaya_hosting`,`pic_teknik`,`no_telp`,`email`,`url`,`tgl_setup`,`harga_aplikasi`,`history`,`status_pembayaran`,`created_at`,`updated_at`) values 
(1,1,1,'Nomor SPK 1','2043-01-08','KOPERASI PRIMKOPAU PUKADARA SU','Dwi','Jl. Pangkalan TNI AU Suryadarma Kabupaten Subang',30,300000,'Ridwan','081802077680','ridwanpurnomo2007@gmail.com ','primkopau.smartcoop.id','2017-10-10',20000000,'Cash','LUNAS',NULL,NULL),
(2,1,1,'Nomor SPK 2','2043-01-09','KOPERASI JASA BAITUTTAMWIL BER','Yudi','Jl. Ciganitri No. 2 Desa Cipagalo Kec. Bojong Soang Kab. Bandung, Jawa Barat',31,300000,'Zein Zayyin','081809962350','zeinzayyin@gmail.com ','berkahumat.smartcoop.id','2017-10-11',20000000,'Cash','LUNAS',NULL,NULL),
(3,2,1,'Nomor SPK 3','2043-02-00','KOPERASI JASA KEUANGAN SYARIAH','Rayi','Jl. Maleer V no. 20, Kecamatan Batununggal Bandung, Jawa Barat',32,300000,'Zein Zayyin','081809962350','zeinzayyin@gmail.com ','eltazkiyah.smartcoop.id','2017-10-12',20000000,'Cash','LUNAS',NULL,NULL),
(4,2,1,'Nomor SPK 4','2043-02-01','KOPERASI WANITA BINANGKIT ','Anugrah',' jl. Soekarno-Hatta 705 Bandung, Jawa Barat',33,300000,'Robbyana','081573228162','robyanamer@gmail.com ','binangkit.smartcoop.id','2017-10-13',20000000,'Cash','LUNAS',NULL,NULL),
(5,2,1,'Nomor SPK 5','2043-02-02','KOPERASI PUSAT KOPERASI WANITA','Ilham','JL. Soekarno-Hatta Bandung, Jawa Barat',34,300000,'Ana','081394415234','','puskowan.smartcoop.id','2017-10-14',20000000,'Cash','LUNAS',NULL,NULL),
(6,1,1,'Nomor SPK 6','2043-02-03','KOPERASI ENERGI MULIA NUSANTAR','Jaya','Ruko Kebayoran Arcade 5 Blok F.3/15 Lt. 3 CBD Kebayoran Boulevard Bintaro Jaya Sektor 7 Tangerang Selatan, Banten',35,300000,'Beta Ayler','08118000567','aylerb@gmail.com','ken.smartcoop.id','2017-10-15',20000000,'Cash','LUNAS',NULL,NULL),
(7,1,1,'Nomor SPK 7','2043-02-04','KOPERASI JASA RANCAGE JAWA BAR','Kusuma','JL. SOEKARNO-HATTA NO. 729 C',36,300000,'Tuti / Wawat','081320528333','','rancage.smartcoop.id','2017-10-16',20000000,'Cash','LUNAS',NULL,NULL),
(8,2,1,'Nomor SPK 8','2043-02-05','KOPERASI PEGAWAI BPKP PROVINSI','Robby','JL. Raya Cibereum no. 50, Bandung. Jawa Barat',37,300000,'Wido / Elyas','08128401694','smart_rachmansyah@hotmail.com','bpkpjabar.smartcoop.id','2017-10-17',20000000,'Cash','LUNAS',NULL,NULL),
(9,2,1,'Nomor SPK 9','2043-02-06','KOPERASI KOPWANTERA BEKASI','Darwis','Taman Cikunir Indah Blok B8/5 - RT 03 RW 11, Kelurahan Jakamulya, Kecamatan Bekasi Selatan, Kota Bekasi Jawa Barat',38,300000,'Yatti','0811915306','yatti_arief@yahoo.co.id ','kopwantera.smartcoop.id','2017-10-18',20000000,'Cash','LUNAS',NULL,NULL),
(10,1,1,'Nomor SPK 10','2043-02-07','KOPERASI WIRAUSAHA BARU JABAR ','Akbar','jln sukarno hatta no 708 bandung',39,300000,'Nelly / Teti','082121593787','tetiataya@yahoo.co.id ','wjs.smartcoop.id','2017-10-19',20000000,'Cash','LUNAS',NULL,NULL),
(11,2,1,'Nomor SPK 11','2043-02-08','KOPERASI MIKRO SUMUT','Ahmad','Jl. Teuku Umar No 59c Tanjungbalai - Sumatera Utara',40,300000,'Rahmat Panjaitan','085277959928','','micro.smartcoop.id','2017-10-20',20000000,'Cash','LUNAS',NULL,NULL),
(12,1,1,'Nomor SPK 12','2043-02-09','KOPERASI KSU KSB MALANG','Dimyati','Malang',41,300000,'Denice/ Rosa','08123476084','','ksb.smartcoop.id','2017-10-21',20000000,'Cash','LUNAS',NULL,NULL),
(13,1,1,'Nomor SPK 13','2043-03-00','KOPERASI KARYAWAN PT. RODA PRI','Doddy','JL. Gatot Subroto KM 04, Komplek Kalisabi, Jatiuwung Cibodas Tangerang Banten',42,300000,'debby','087880891111','debby_dwipada@rpl.co.id','rpl.smartcoop.id','2017-10-22',20000000,'Cash','LUNAS',NULL,NULL),
(14,1,1,'Nomor SPK 14','2043-03-01','KOPERASI CATUR BHAKTI” KANWIL ','Regi','Jalan Otista Raya No. 53-55 Jakarta Timur',43,300000,'Pauli','081296554933','t.monang@gmail.com','caturbhakti.smartcoop.id','2017-10-23',20000000,'Cash','LUNAS',NULL,NULL),
(15,1,1,'Nomor SPK 15','2043-03-02','KOPERASI CIPTA INTI SAMUDRA','Shofi','Sundamekar, Cisitu, Sumedang',44,300000,'Giri','082121601888','','cis.smartcoop.id','2017-10-24',20000000,'Cash','LUNAS',NULL,NULL),
(16,1,1,'Nomor SPK 16','2043-03-03','KOPERASI KARYAWAN RSBD JAKARTA','Asri','Jalan Duren Tiga Raya No.5, RT.1/RW.1, RT.1/RW.1, Pancoran, Jakarta Selatan, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta',45,300000,'Renold','081919004545','','rsbd.smartcoop.id','2017-10-25',20000000,'Cash','LUNAS',NULL,NULL),
(17,1,1,'Nomor SPK 17','2043-03-04','KOPERASI JASA RAHARJA','Ikhsan','Jl. Soekarno-Hatta No.689 A, Jatisari, Buahbatu, Kota Bandung, Jawa Barat 40286',46,300000,'Adit','081320654643','','jasaraharja.smartcoop.id','2017-10-26',20000000,'Cash','LUNAS',NULL,NULL),
(18,2,1,'Nomor SPK 18','2043-03-05','KOPERASI KONSUMEN KUNCI SUKSES','Rian','Jl Prof Dr. Satrio Kav 18 Setiabudi Karet Kuningan Jakarta Selatan 12940 ',47,300000,'Andriansyah','0811854021','','k3sb.smartcoop.id','2017-10-27',20000000,'Cash','LUNAS',NULL,NULL),
(19,1,1,'Nomor SPK 19','2043-03-06','KOPERASI KARYAWAN PT. ARGAPURA','Rijwan','Jl. Manis II No. 17, Kawasan Industri Manis, Tagerang 15136',48,300000,'Rully','08121856529','','argapura.smartcoop.id','2017-10-28',20000000,'Cash','LUNAS',NULL,NULL),
(20,2,1,'Nomor SPK 20','2043-03-07','KOPERASI LAMALEPA BATAM','Sandy','KOMP.TOWNHOUSE  GLORY HOME BLOK A1 NO.07 KEL.SADAI KEC. BENGKONG KOTA BATAM PROV. KEPRI',49,300000,'Sukma','081268407052','','lamalepa.smartcoop.id','2017-10-29',20000000,'Cash','BELUM LUNAS',NULL,NULL),
(21,1,1,'Nomor SPK 21','2043-03-08','BMT NU PESAWERAN','Reza','Jl Ahmad Yani, Kel Bagelen Kec gedong Tataan, Kab Pesawaran, Lampung 35366',50,300000,'M Ujang Bihaqi, S.E.I','085647645911','','pesaweran.smartcoop.id','2017-10-30',20000000,'Cash','LUNAS',NULL,NULL),
(22,1,1,'Nomor SPK 22','2043-03-09','KOPERASI KARYAWAN PT SEIWA IND','Purwa','Jl Lombok 1 Blok M2-2 MM2100, Cikarang Barat, Bekasi',51,300000,'Muendang','081283981245/083','','seiwa.smartcoop.id','2017-10-31',20000000,'Cash','BELUM LUNAS',NULL,NULL),
(23,1,1,'Nomor SPK 23','2043-04-00','KOPERASI DEWI SRI DEWI DINAS T','Farhat','Jl. Surapati No. 71 Kota Bandung',52,300000,'Duki','85624073969','','dewisri.smartcoop.id','0000-00-00',20000000,'Cash','LUNAS',NULL,NULL),
(24,1,1,'Nomor SPK 24','2043-04-01','KOPERASI KPRI “Gardenia” Balai','Dodi','Balai Konservasi Tumbuhan Kebun Raya Cibodas - LIPI',53,300000,'Agus Darmawan, S.Kom','087721475253','','kprigardenia.smartcoop.id','0000-00-00',20000000,'Cash','BELUM LUNAS',NULL,NULL),
(25,1,1,'Nomor SPK 25','2043-04-02','KOPERASI ASTA SARI AMERTHA (AS','Ida','Jl. Pantai Batu Bolong 99 Canggu – Kuta Utara, Badung - Bali',54,300000,'I Wayan Miarta','08970218956','','asa.smartcoop.id','0000-00-00',20000000,'Cash','BELUM LUNAS',NULL,NULL),
(26,1,1,'Nomor SPK 26','2043-04-03','KOPERASI KARYAWAN PT TUGU REAS','Zulfiqro','Gd. Tugure, jl KH Wahid Hasyim No 4a, Kebon Sirih - Menteng, Jakarta Pusat 10340',55,300000,'Sri Tuti','089658951328','','pttuguindonesia.smartcoop.id','0000-00-00',20000000,'Cash','BELUM LUNAS',NULL,NULL),
(27,1,1,'Nomor SPK 27','2043-04-04','KOPERASI NANDA TRANS','Adi','Jl. Batuyang Gang Pipit II No. 8 Batubulan, Gianyar - Bali',56,300000,'I Wayan Miarta','08970218956','','nanda.smartcoop.id','0000-00-00',20000000,'Cash','BELUM LUNAS',NULL,NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(7,'2014_10_12_000000_create_users_table',1),
(8,'2014_10_12_100000_create_password_resets_table',1),
(9,'2019_04_08_073222_create_jenis_koperasi_table',1),
(10,'2019_04_08_073343_create_koperasi_table',1),
(11,'2019_04_08_084555_create_rekap_file_table',1),
(12,'2019_04_09_063835_create_permission_tables',1);

/*Table structure for table `model_has_permissions` */

DROP TABLE IF EXISTS `model_has_permissions`;

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `model_has_permissions` */

/*Table structure for table `model_has_roles` */

DROP TABLE IF EXISTS `model_has_roles`;

CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `model_has_roles` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

/*Table structure for table `rekap_file` */

DROP TABLE IF EXISTS `rekap_file`;

CREATE TABLE `rekap_file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `koperasi_id` int(10) unsigned NOT NULL,
  `users_id` tinyint(3) unsigned NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rekap_file_koperasi_id_foreign` (`koperasi_id`),
  KEY `rekap_file_users_id_foreign` (`users_id`),
  CONSTRAINT `rekap_file_koperasi_id_foreign` FOREIGN KEY (`koperasi_id`) REFERENCES `koperasi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rekap_file_users_id_foreign` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `rekap_file` */

/*Table structure for table `role_has_permissions` */

DROP TABLE IF EXISTS `role_has_permissions`;

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_has_permissions` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`guard_name`,`created_at`,`updated_at`) values 
(1,'Direktur','web','2019-04-10 10:30:04','2019-04-10 10:30:04'),
(2,'Tamu','web','2019-04-10 10:30:10','2019-04-10 10:30:10'),
(3,'Dinas','web','2019-04-10 10:30:14','2019-04-10 10:30:14');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Dwi Yudi Rayi Anugrah','dwiyudirayia789@gmail.com',NULL,'$2y$10$HpDLeXsVn9A9ncnDtT8TF.geG9vW.cT4lJG2LmA9dNZ3saJ0ho7TO','ysscWab1R5mbeLYYva8inDYG9ijkxZ6PeX68ZmwaJiRQpeNPh6y3B2H2yYit','2019-04-10 07:51:59','2019-04-10 07:51:59');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
